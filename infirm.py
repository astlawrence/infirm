#!/usr/bin/env python3

"""Usage:
  infirm.py [-f|-t] [-r] FIRMWARE
  infirm.py (-i | --init) YEAR
  infirm.py (-l | --locals)
  infirm.py (-c | --clear) [binaries|locals|cve]
  infirm.py (-u | --update) [cve|binaries]
  infirm.py (-h | --help)
  infirm.py --version

Options:
  -f                Perform a fast analysis using YARA rules.
  -t                Perform a thorough analysis using precomputed binary data (default).
  -r                Retain analyzed binary metadata in local database.
  -i --init         Initialize the database with NVD CVE data starting from the year provided to current.
  -l --locals       Rerun analysis on binaries in local database.
  -c --clear        Delete all data in the binaries, locals, or cve database tables.
  -u --update       Update CVE or binaries data to current.
  -h --help         Show this screen.
  --version         Show version.
"""

# Update options: CVE data, binary data, yara rules

from docopt import docopt
from subprocess import call
from functools import partial
from prettytable import PrettyTable
from fuzzywuzzy import fuzz
import os
import sys
import json
import hashlib
import sqlite3
import magic
import urllib.request
import shutil
import datetime
import yara


#pip3 install docopt filemagic prettytable yara-python fuzzywuzzy python-Levenshtein
#apt-get install sqlite3 binwalk gzip

#https://pypi.python.org/pypi/ssdeep - hash fuzzies
#  - add to thorough lookup and re-analysis of locals

#https://github.com/seatgeek/fuzzywuzzy - string fuzzies


# Print usage/help.
def print_help():
	print("infirm.py [-f|-t] [-r] FIRMWARE")
	print("  infirm.py (-i | --init) YEAR")
	print("  infirm.py (-l | --locals)")
	print("  infirm.py (-c | --clear) [locals|cve]")
	print("  infirm.py (-u | --update) [cve|binaries]")
	print("  infirm.py (-h | --help)")
	print("  infirm.py --version\n")
	print("Options:")
	print("  -f             Perform a fast analysis using YARA rules.")
	print("  -t             Perform a thorough analysis using precomputed binary data (default).")
	print("  -r             Retain analyzed binary metadata in local database.")
	print("  -i --init      Initialize the database with NVD CVE data starting from the year provided to current.")
	print("  -l --locals    Rerun analysis on binaries in locals database.")
	print("  -c --clear     Delete all data in the locals or cve database tables.")
	print("  -u --update    Update CVE or binaries data to current.")
	print("  -h --help      Show this screen.")
	print("  --version      Show version.")


# SHA256 file hash.
def sha256sum(b):
	# Open binary file.
	with open(b, mode='rb') as f:
		h = hashlib.sha256()
		# Compute SHA256 hash.
		for buf in iter(partial(f.read, 128), b''):
			h.update(buf)
	return h.hexdigest()


# Close database.
def close_database(conn):
	try:		
		conn.close()
	except Exception as e:
		print(e)


# Compile YARA rules.
def compile_yara(root):
	rules_path = os.path.join(root, "rules.yar")
	rules = yara.compile(rules_path)
	return rules
	

# YARA match callback.
def yara_callback(data):
	#print(data)
	#print(type(data))
	#print(data['meta'])
	return yara.CALLBACK_CONTINUE
	

# Fast Lookup.
def fast_lookup(rules, name):
	matches = rules.match(name, callback=yara_callback, which_callbacks=yara.CALLBACK_MATCHES)
	if matches:
		product = matches[0].meta['cve_name']
		ver = matches[0].meta['version']
						
		return product, ver		
	else:
		return None, None
		
	
# Query binaries database table.
def query_binaries(h):
	safe_select = (h,)
	try:
		c.execute('SELECT * FROM binaries WHERE hash=?', safe_select)
		return c.fetchone()
	except Exception as e:
		print(e)


# Query locals database table.
def query_locals(h):
	try:
		if h:
			c.execute('SELECT * FROM locals WHERE hash=?', (h,))
		else:
			c.execute('SELECT * FROM locals')
			
		return c.fetchall()
	except Exception as e:
		print(e)


# Process CVE data.
def process_cve(conn, c, year):
	# Open CVE JSON files.
	with open("nvdcve-1.0-" + str(year) + ".json", 'r') as data:
		cves = json.load(data)
			
	# Process all CVEs for the year.
	for i in range(0, len(cves["CVE_Items"])):
		cve_id = ""
		cve_desc = ""
		cwe = []
		cvss3 = "none"
		cvss2 = "none"
		last_modified = ""
		product_name = ""
		product_versions = []
		product_versions_str = ""
		
		# Get CVE ID.
		cve_id = cves['CVE_Items'][i]['cve']['CVE_data_meta']['ID']
		
		# Get CVE description.
		cve_desc = cves['CVE_Items'][i]['cve']['description']['description_data'][0]['value']
				
		# Get the CVSS v3 and v2 metrics.
		try:
			cvss3 = cves['CVE_Items'][i]['impact']['baseMetricV3']['cvssV3']['baseScore']
		except:
			pass
				
		try:
			cvss2 = cves['CVE_Items'][i]['impact']['baseMetricV2']['cvssV2']['baseScore']
		except:
			pass
		
		# Get last modified timestamp.
		try:
			last_modified = cves['CVE_Items'][i]['lastModifiedDate']
		except:
			last_modified = "unknown"
			pass
					
		# Get listed CWEs.
		for j in range(0, len(cves['CVE_Items'][i]['cve']['problemtype']['problemtype_data'])):
			for k in range(0, len(cves['CVE_Items'][i]['cve']['problemtype']['problemtype_data'][j]['description'])):
				cwe.append(cves['CVE_Items'][i]['cve']['problemtype']['problemtype_data'][j]['description'][k]['value'])
		cwe_str = ", ".join(cwe)		
					
		# Get products and versions.
		for j in range(0, len(cves['CVE_Items'][i]['cve']['affects']['vendor']['vendor_data'])):
			
			for k in range(0, len(cves['CVE_Items'][i]['cve']['affects']['vendor']['vendor_data'][j]['product']['product_data'])):
				product_name = cves['CVE_Items'][i]['cve']['affects']['vendor']['vendor_data'][j]['product']['product_data'][k]['product_name']
										
				for l in range(0, len(cves['CVE_Items'][i]['cve']['affects']['vendor']['vendor_data'][j]['product']['product_data'][k]['version']['version_data'])):
					product_versions.append(cves['CVE_Items'][i]['cve']['affects']['vendor']['vendor_data'][j]['product']['product_data'][k]['version']['version_data'][l]['version_value'])
									
				product_versions_str = ", ".join(product_versions)
					
				
		# Check if CVE exists in the cve database table.
		safe_select = (str(cve_id),)
		try: 
			c.execute('SELECT * FROM cve WHERE id = ?', safe_select)
			row = c.fetchall()
		except Exception as e:
			print(e)
						
		# CVE exists in cve database table.
		if row:
			# Check last modified date and update record if they do not match.
			if last_modified not in row[0][7]:
				print("\t" + str(cve_id) + " last modified different.")
						
				# Update CVE record.
				safe_select = (str(last_modified), str(cvss2), str(cvss3), str(cwe_str), str(cve_desc), str(product_versions_str), str(product_name), str(cve_id),)
				try:
					c.execute('UPDATE cve SET lastModified = ?, cvss2 = ?, cvss3 = ?, cwe = ?, desc = ?, version = ?, product = ? WHERE id = ?', safe_select)
				except Exception as e:
					print(e)
		else:
			# Insert into the CVE database table.
			sql = []
			sql.append(str(product_name))
			sql.append(str(product_versions_str))
			sql.append(str(cve_id))
			sql.append(str(cve_desc))
			sql.append(str(cwe_str))
			sql.append(str(cvss3))
			sql.append(str(cvss2))
			sql.append(str(last_modified))
							
			try:
				c.execute('INSERT INTO cve VALUES (?,?,?,?,?,?,?,?)', sql)
			except Exception as e:
				print(e)
			
	# Commit to cve database table.
	try:
		conn.commit()
	except Exception as e:
		print(e)


# Download CVE data.
def download_cve(year, now):
	for y in range(year, (now.year + 1)):
		url = "https://static.nvd.nist.gov/feeds/json/cve/1.0/nvdcve-1.0-" + str(y) + ".json.gz"
		f = "nvdcve-1.0-" + str(y) + ".json.gz"
	
		with urllib.request.urlopen(url) as response, open(f, 'wb') as out:
			try:
				shutil.copyfileobj(response, out)
			except Exception as e:
				print(e)
				sys.exit()
					
		print("[+] Extracting " + f + "...")
			
		try:
			call(['gunzip', '-f', f])
		except Exception as e:
			print(e)
			sys.exit()
	

# Update config database table.
def update_config(conn, c, year):
	try:
		c.execute('SELECT cveYear FROM config')
		row = c.fetchone()
	except Exception as e:
		print(e)
		
	if row is None:
		try:
			c.execute('INSERT INTO config VALUES (?)', (str(year),))
		except Exception as e:
			print(e)
	else:
		try:
			c.execute('UPDATE config SET cveYear = ?', (str(year),))
		except Exception as e:
			print(e)
			
	try:
		conn.commit()
	except Exception as e:
		print(e)
	

# Query config database table.
def query_config():
	# Get initialized CVE year from config database table.
	try:
		c.execute('SELECT cveYear FROM config')
		row = c.fetchone()
	except Exception as e:
		print(e)
			
	if row:
		return int(row[0])
	else:
		return None
		

# Main
if __name__ == '__main__':
	arguments = docopt(__doc__, version='infirm 0.1')
	
	# Open the database.
	db_root = os.path.dirname(os.path.abspath(__file__))
	db_path = os.path.join(db_root, "infirm.db")
	try:
		conn = sqlite3.connect(db_path)
		c = conn.cursor()
	except Exception as e:
		print("\n[-] " + e)
		sys.exit()

	# Get current year.
	now = datetime.datetime.now()
	
	# Perform initialization.
	if arguments['--init']:
		year = int(arguments['YEAR'])
		
		# Check if database initialized previously.
		if query_config() is not None:
			print("\n[-] Database intiliazed previously.\n")
			sys.exit()
		
		# Print status.
		print("\n[+] Initializing the database...")
		print("[+] Downloading CVE data from " + str(year) + "-" + str(now.year) + "...")

		# Download CVE data from NVD.
		download_cve(year, now)
				
		# Process CVE data.
		for y in range(year, (now.year + 1)):
			print("[+] Parsing CVE data for " + str(y) + "...")
			process_cve(conn, c, y)
		
		# Update config database table.
		update_config(conn, c, year)
		
		# Close database.
		close_database(conn)
		
		print("[+] Database initialization complete.\n")

	elif arguments['--update']:
		if arguments['binaries']:
			# Get initialized CVE year from config database table.
			year = query_config()
			if year is None:
				print("\n[-] Database has not been initialized.\n")
				sys.exit()
			
			# Print status.
			#print("\n[+] Updating the binaries database table...")
			print("\nTo be implemented in a future release.\n")
			
		elif arguments['cve']:
			# Get initialized CVE year from config database table.
			year = query_config()
			if year is None:
				print("\n[-] Database has not been initialized.\n")
				sys.exit()
					
			# Print status.
			print("\n[+] Updating the CVE database table for " + str(year) + "-" + str(now.year) + "...")
			
			# Download CVE data.
			download_cve(year, now)

			# Process CVE data.
			for y in range(year, (now.year + 1)):
				print("[+] Parsing CVE data for " + str(y) + "...")
				process_cve(conn, c, year)

			# Close database.
			close_database(conn)
			
			# Print status.
			print("[+] Database update complete.\n")
		else:
			print_help()
			
			# Close database.
			close_database(conn)
		
	elif arguments['--locals']:
		# Instantiate output tables.
		t = PrettyTable(['Binary', 'Firmware', 'Version', 'Arch', 'Options', 'CVE', 'CWE', 'CVSSv3', 'CVSSv2'])
		z = PrettyTable(['Match %', 'Binary', 'Firmware', 'Version', 'CVE', 'CWE', 'CVSSv3', 'CVSSv2'])
		
		# Prepare YARA rules.
		rules = compile_yara(db_root)
		
		# Retrieve locals database table.
		c.execute('SELECT * FROM locals')
		locals_data = c.fetchall()	
									
		for locals_row in locals_data:
			# Update locals database table lastAnalysis field.
			safe_select = (now, locals_row[0],)
			c.execute('UPDATE locals SET lastAnalysis = ? WHERE hash = ?', safe_select)
			conn.commit()
		
			# If locals database table field 'product' exists, it means a firmware binary was
			# a previous match using fast or thorough methods, otherwise it will be None and 
			# a thorough lookup should be performed followed by a YARA rules analysis if no
			# thorough match was found. 
			if locals_row[6] in "None":				
				# Compare locals hash to binaries hash, if match, process CVE data, otherwise, fast lookup.
				c.execute('SELECT * FROM binaries WHERE hash = ?', (locals_row[0],))
				binary_row = c.fetchone()

				# Thorough lookup.
				if binary_row:
					# Update locals database.
					safe_select = (binary_row[1], binary_row[3], locals_row[0],)
					c.execute('UPDATE locals SET product = ?, version = ? WHERE hash = ?', safe_select)
					
					# Commit.				
					conn.commit()
					
					c.execute("SELECT * FROM cve WHERE product LIKE ('%'||?||'%')", (binary_row[1],))
					cves = c.fetchall()
					
					if cves:
						for cve in cves:
							# Check if binary version matches CVE affected versions.
							if (str(binary_row[3]) in str(cve[1]) or ("*" in str(cve[1])) or ("-" in str(cve[1]))):
								t.add_row([locals_row[2], locals_row[1], binary_row[3], binary_row[5], binary_row[4], cve[2], cve[4], cve[5], cve[6]])
				
				# Fast lookup.
				else:
					# Check if file still exists on disk and then run YARA rules against it.
					if (os.path.isfile(locals_row[3])):
						product, ver = fast_lookup(rules, locals_row[3])
						
						# YARA rule match.
						if product is not None:
							# Update locals database.
							safe_select = (product, ver, locals_row[3],)
							c.execute('UPDATE locals SET product = ?, version = ? WHERE path = ?', safe_select)
							
							# Commit.				
							conn.commit()
							
							c.execute("SELECT * FROM cve WHERE product LIKE ('%'||?||'%')", (product,))
							cves = c.fetchall()
							
							if cves:
								for cve in cves:
									# Check if binary version matches CVE affected versions.
									if (str(ver) in str(cve[1])) or ("*" in str(cve[1])) or ("-" in str(cve[1])):
										# Add an entry to the report.
										t.add_row([locals_row[2], locals_row[1], ver, "-", "-", cve[2], cve[4], cve[5], cve[6]])
					else:
						c.execute('SELECT * FROM cve')
						cves = c.fetchall()
						
						for cve in cves:
							# Perform a fuzzy string match between CVE product and locals database table binary field.
							fuzzy_match = fuzz.partial_ratio(str(cve[0]), str(locals_row[2]))
							if (fuzzy_match > 90):
								# If a partial ratio match exceeds the threshold, search for locals database table binary field in CVE description.
								if (str(locals_row[2]) in cve[3]):
									z.add_row([str(fuzzy_match), locals_row[2], locals_row[1], cve[1], cve[2], cve[4], cve[5], cve[6]])
				
			else:
				c.execute("SELECT * FROM cve WHERE product LIKE ('%'||?||'%')", (locals_row[6],))
				cves = c.fetchall()
				
				for cve in cves:
					# Check if binary version matches CVE affected versions.
					if (str(locals_row[7]) in str(cve[1])) or ("*" in str(cve[1])) or ("-" in str(cve[1])):
						# Add an entry to the report.
						t.add_row([locals_row[2], locals_row[1], locals_row[7], "-", "-", cve[2], cve[4], cve[5], cve[6]])
						
		# Print table.
		print("\n[+] High confidence matches:")
		print(t)
		print("\n[+] Potential matches based on fuzzy string matching:")
		print(z)
		
		# Close database.
		close_database(conn)		
		
	elif arguments['FIRMWARE']:
		# Check if database has been initialized.
		if query_config() is None:
			print("\n[-] Database has not been initialized.\n")
			sys.exit()
			
		# Print patience message.
		print("\n[+]  Please be patient; this may take a while...\n")
		
		# Get firmware file name.
		firmware = arguments['FIRMWARE']
		
		# Run a binwalk recursive extraction on the firmware.
		call(['binwalk', '-Meq', firmware]) 
				
		# Prepare paths.
		root_dir = '_' + firmware + '.extracted'
		os.chdir(root_dir)
		
		# Instantiate output table.
		t = PrettyTable(['Binary', 'SHA256', 'Version', 'Arch', 'Options', 'CVE', 'CWE', 'CVSSv3', 'CVSSv2'])

		# Prepare YARA rules.
		if arguments['-f']:
			rules = compile_yara(db_root)

		# Parse each extracted file.
		for root, dirs, files in os.walk("."):
			for name in files:
				product = ""
				ver = ""
				
				# Check if each file is ELF executable using libmagic.
				# YARA can do this - consider removing magic
				with magic.Magic() as m:
					f = m.id_filename(os.path.join(root, name))
					if ("executable" in f) and ("ELF" in f):
						# Hash each ELF executable.
						h = sha256sum(os.path.join(root, name))
						
						# FAST LOOKUP
						if arguments['-f']:
							# Perform fast lookup.
							product, ver = fast_lookup(rules, os.path.abspath(os.path.join(root, name)))

							if product and ver:
								c.execute("SELECT * FROM cve WHERE product LIKE ('%'||?||'%')", (product,))
								rows = c.fetchall()
								for row in rows:
									# Check if binary version matches CVE affected versions.
									if (str(ver) in str(row[1])) or ("*" in str(row[1])) or ("-" in str(row[1])):
										# Add an entry to the report.
										t.add_row([name, h, ver, "-", "-", row[2], row[4], row[5], row[6]])
										
						# THOROUGH LOOKUP	
						if not arguments['-f']:
							# Perform thorough lookup by binary hash value.
							row = query_binaries(h)
								
							if row:
								product = row[1]
								ver = row[3]
									
								c.execute("SELECT * FROM cve WHERE product LIKE ('%'||?||'%')", (product,))
								rows = c.fetchall()		
									
								# Check CVE data for vulnerabilities.
								for r in rows:
									# Check if binary version matches CVE affected versions.
									if (str(ver) in str(r[1])) or ("*" in str(r[1])) or ("-" in str(r[1])):
										# Add an entry to the report.
										t.add_row([name, h, ver, row[5], row[4], r[2], r[4], r[5], r[6]])

						# Retain binary data in locals database table.
						if arguments['-r']:
							# IMPLEMENT CHECK FOR HASH, IF NOT FOUND, INSERT, OTHERWISE UPDATE LASTANALYSIS
							try:
								rows = query_locals(h)
								
								if not rows:
									# Hash not found in locals, insert into table.
									sql = []
									sql.append(str(h))
									sql.append(str(firmware))
									sql.append(str(name))
									sql.append(str(os.path.abspath(os.path.join(root, name))))
									sql.append(str(now))
									sql.append(str(now))
									
									try:
										sql.append(str(product))
										sql.append(str(ver))
									except:
										sql.append(None)
										sql.append(None)
											
									c.execute('INSERT INTO locals VALUES (?,?,?,?,?,?,?,?)', sql)
								else:
									# Hash found in locals, update last analysis field.
									safe_select = (now, h,)
									c.execute('UPDATE locals SET lastAnalysis = ? WHERE hash = ?', safe_select)
							except Exception as e:
								print(e)
						
						# Commit.				
						conn.commit()
						
		# Print table.
		print(t)
		
		# Close database.
		close_database(conn)
	
	elif arguments['--clear']:
		if arguments['locals']:
			# Delete locals database table rows.
			c.execute('DELETE FROM locals')
			conn.commit()
			close_database(conn)
			
			print("\n[+] Cleared locals database table.\n")
		
		elif arguments['cve']:
			# Delete CVE database table rows and subsequently reset config.
			c.execute('DELETE FROM cve')
			c.execute('DELETE FROM config')
			conn.commit()
			close_database(conn)
			
			print("\n[+] Cleared CVE database table.\n")
			
		else:
			print_help()
			
			# Close database.
			close_database(conn)
	
	else:
		print_help()
		
		# Close database.
		close_database(conn)
