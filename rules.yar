private rule busybox
{
	meta:
		name = "BusyBox"
		cve_name = "busybox"
		open_source = true
	strings:
		$a = /BusyBox.{1}v\d+\.\d+(.\d+)?/ nocase ascii wide
	condition:
		$a
}
private rule bzip2
{
	meta:
		name = "bzip2"
		cve_name = "bzip2"
		open_source = true
	strings:
		$a = /bzip2-(\d+\.\d+\.\d+)/ nocase ascii wide
	condition:
		$a
}
private rule dnsmasq 
{
	meta:
		name = "dnsmasq"
		cve_name = "dnsmasq"
		open_source = true
	strings:
		$a = /dnsmasq-(\d+.\d+){1}/ nocase ascii wide
	condition:
		$a
}
rule dnsmasq_2_25 : v2_25
{
	meta:
		name = "dnsmasq"
		cve_name = "dnsmasq"
		open_source = true
		version = "2.25"
	strings:
		$a = "2.25" ascii wide
	condition:
		$a and dnsmasq
}
rule dnsmasq_2_26 : v2_26
{
	meta:
		name = "dnsmasq"
		cve_name = "dnsmasq"
		open_source = true
		version = "2.26"
	strings:
		$a = "2.26" ascii wide
	condition:
		$a and dnsmasq
}
rule dnsmasq_2_27 : v2_27
{
	meta:
		name = "dnsmasq"
		cve_name = "dnsmasq"
		open_source = true
		version = "2.27"
	strings:
		$a = "2.27" ascii wide
	condition:
		$a and dnsmasq
}
rule dnsmasq_2_28 : v2_28
{
	meta:
		name = "dnsmasq"
		cve_name = "dnsmasq"
		open_source = true
		version = "2.28"
	strings:
		$a = "2.28" ascii wide
	condition:
		$a and dnsmasq
}
rule dnsmasq_2_29 : v2_29
{
	meta:
		name = "dnsmasq"
		cve_name = "dnsmasq"
		open_source = true
		version = "2.29"
	strings:
		$a = "2.29" ascii wide
	condition:
		$a and dnsmasq
}
rule dnsmasq_2_30 : v2_30
{
	meta:
		name = "dnsmasq"
		cve_name = "dnsmasq"
		open_source = true
		version = "2.30"
	strings:
		$a = "2.30" ascii wide
	condition:
		$a and dnsmasq
}
rule dnsmasq_2_31 : v2_31
{
	meta:
		name = "dnsmasq"
		cve_name = "dnsmasq"
		open_source = true
		version = "2.31"
	strings:
		$a = "2.31" ascii wide
	condition:
		$a and dnsmasq
}
rule dnsmasq_2_32 : v2_32
{
	meta:
		name = "dnsmasq"
		cve_name = "dnsmasq"
		open_source = true
		version = "2.32"
	strings:
		$a = "2.32" ascii wide
	condition:
		$a and dnsmasq
}
rule dnsmasq_2_33 : v2_33
{
	meta:
		name = "dnsmasq"
		cve_name = "dnsmasq"
		open_source = true
		version = "2.33"
	strings:
		$a = "2.33" ascii wide
	condition:
		$a and dnsmasq
}
rule dnsmasq_2_34 : v2_34
{
	meta:
		name = "dnsmasq"
		cve_name = "dnsmasq"
		open_source = true
		version = "2.34"
	strings:
		$a = "2.34" ascii wide
	condition:
		$a and dnsmasq
}
rule dnsmasq_2_35: v2_35
{
	meta:
		name = "dnsmasq"
		cve_name = "dnsmasq"
		open_source = true
		version = "2.35"
	strings:
		$a = "2.35" ascii wide
	condition:
		$a and dnsmasq
}
rule dnsmasq_2_36: v2_36
{
	meta:
		name = "dnsmasq"
		cve_name = "dnsmasq"
		open_source = true
		version = "2.36"
	strings:
		$a = "2.36" ascii wide
	condition:
		$a and dnsmasq
}
rule dnsmasq_2_37 : v2_37
{
	meta:
		name = "dnsmasq"
		cve_name = "dnsmasq"
		open_source = true
		version = "2.37"
	strings:
		$a = "2.37" ascii wide
	condition:
		$a and dnsmasq
}
rule dnsmasq_2_38 : v2_38
{
	meta:
		name = "dnsmasq"
		cve_name = "dnsmasq"
		open_source = true
		version = "2.38"
	strings:
		$a = "2.38" ascii wide
	condition:
		$a and dnsmasq
}
rule dnsmasq_2_39 : v2_39
{
	meta:
		name = "dnsmasq"
		cve_name = "dnsmasq"
		open_source = true
		version = "2.39"
	strings:
		$a = "2.39" ascii wide
	condition:
		$a and dnsmasq
}
rule dnsmasq_2_40 : v2_40
{
	meta:
		name = "dnsmasq"
		cve_name = "dnsmasq"
		open_source = true
		version = "2.40"
	strings:
		$a = "2.40" ascii wide
	condition:
		$a and dnsmasq
}
rule dnsmasq_2_41 : v2_41
{
	meta:
		name = "dnsmasq"
		cve_name = "dnsmasq"
		open_source = true
		version = "2.41"
	strings:
		$a = "2.41" ascii wide
	condition:
		$a and dnsmasq
}
rule dnsmasq_2_42 : v2_42
{
	meta:
		name = "dnsmasq"
		cve_name = "dnsmasq"
		open_source = true
		version = "2.42"
	strings:
		$a = "2.42" ascii wide
	condition:
		$a and dnsmasq
}
rule dnsmasq_2_43 : v2_43
{
	meta:
		name = "dnsmasq"
		cve_name = "dnsmasq"
		open_source = true
		version = "2.43"
	strings:
		$a = "2.43" ascii wide
	condition:
		$a and dnsmasq
}
rule dnsmasq_2_44 : v2_44
{
	meta:
		name = "dnsmasq"
		cve_name = "dnsmasq"
		open_source = true
		version = "2.44"
	strings:
		$a = "2.44" ascii wide
	condition:
		$a and dnsmasq
}
rule dnsmasq_2_45 : v2_45
{
	meta:
		name = "dnsmasq"
		cve_name = "dnsmasq"
		open_source = true
		version = "2.45"
	strings:
		$a = "2.45" ascii wide
	condition:
		$a and dnsmasq
}
rule dnsmasq_2_46 : v2_46
{
	meta:
		name = "dnsmasq"
		cve_name = "dnsmasq"
		open_source = true
		version = "2.46"
	strings:
		$a = "2.46" ascii wide
	condition:
		$a and dnsmasq
}
rule dnsmasq_2_47 : v2_47
{
	meta:
		name = "dnsmasq"
		cve_name = "dnsmasq"
		open_source = true
		version = "2.47"
	strings:
		$a = "2.47" ascii wide
	condition:
		$a and dnsmasq
}
rule dnsmasq_2_48 : v2_48
{
	meta:
		name = "dnsmasq"
		cve_name = "dnsmasq"
		open_source = true
		version = "2.48"
	strings:
		$a = "2.48" ascii wide
	condition:
		$a and dnsmasq
}
rule dnsmasq_2_49 : v2_49
{
	meta:
		name = "dnsmasq"
		cve_name = "dnsmasq"
		open_source = true
		version = "2.49"
	strings:
		$a = "2.49" ascii wide
	condition:
		$a and dnsmasq
}
rule dnsmasq_2_50 : v2_50
{
	meta:
		name = "dnsmasq"
		cve_name = "dnsmasq"
		open_source = true
		version = "2.50"
	strings:
		$a = "2.50" ascii wide
	condition:
		$a and dnsmasq
}
rule dnsmasq_2_51 : v2_51
{
	meta:
		name = "dnsmasq"
		cve_name = "dnsmasq"
		open_source = true
		version = "2.51"
	strings:
		$a = "2.51" ascii wide
	condition:
		$a and dnsmasq
}
rule dnsmasq_2_52 : v2_52
{
	meta:
		name = "dnsmasq"
		cve_name = "dnsmasq"
		open_source = true
		version = "2.52"
	strings:
		$a = "2.52" ascii wide
	condition:
		$a and dnsmasq
}
rule dnsmasq_2_53 : v2_53
{
	meta:
		name = "dnsmasq"
		cve_name = "dnsmasq"
		open_source = true
		version = "2.53"
	strings:
		$a = "2.53" ascii wide
	condition:
		$a and dnsmasq
}
rule dnsmasq_2_54 : v2_54
{
	meta:
		name = "dnsmasq"
		cve_name = "dnsmasq"
		open_source = true
		version = "2.54"
	strings:
		$a = "2.54" ascii wide
	condition:
		$a and dnsmasq
}
rule dnsmasq_2_55 : v2_55
{
	meta:
		name = "dnsmasq"
		cve_name = "dnsmasq"
		open_source = true
		version = "2.55"
	strings:
		$a = "2.55" ascii wide
	condition:
		$a and dnsmasq
}
rule dnsmasq_2_56 : v2_56
{
	meta:
		name = "dnsmasq"
		cve_name = "dnsmasq"
		open_source = true
		version = "2.56"
	strings:
		$a = "2.56" ascii wide
	condition:
		$a and dnsmasq
}
rule dnsmasq_2_57 : v2_57
{
	meta:
		name = "dnsmasq"
		cve_name = "dnsmasq"
		open_source = true
		version = "2.57"
	strings:
		$a = "2.57" ascii wide
	condition:
		$a and dnsmasq
}
rule dnsmasq_2_58 : v2_58
{
	meta:
		name = "dnsmasq"
		cve_name = "dnsmasq"
		open_source = true
		version = "2.58"
	strings:
		$a = "2.58" ascii wide
	condition:
		$a and dnsmasq
}
rule dnsmasq_2_59 : v2_59
{
	meta:
		name = "dnsmasq"
		cve_name = "dnsmasq"
		open_source = true
		version = "2.59"
	strings:
		$a = "2.59" ascii wide
	condition:
		$a and dnsmasq
}
rule dnsmasq_2_60 : v2_60
{
	meta:
		name = "dnsmasq"
		cve_name = "dnsmasq"
		open_source = true
		version = "2.60"
	strings:
		$a = "2.60" ascii wide
	condition:
		$a and dnsmasq
}
rule dnsmasq_2_61 : v2_61
{
	meta:
		name = "dnsmasq"
		cve_name = "dnsmasq"
		open_source = true
		version = "2.61"
	strings:
		$a = "2.61" ascii wide
	condition:
		$a and dnsmasq
}
rule dnsmasq_2_62 : v2_62
{
	meta:
		name = "dnsmasq"
		cve_name = "dnsmasq"
		open_source = true
		version = "2.62"
	strings:
		$a = "2.62" ascii wide
	condition:
		$a and dnsmasq
}
rule dnsmasq_2_63 : v2_63
{
	meta:
		name = "dnsmasq"
		cve_name = "dnsmasq"
		open_source = true
		version = "2.63"
	strings:
		$a = "2.63" ascii wide
	condition:
		$a
}
rule dnsmasq_2_64 : v2_64
{
	meta:
		name = "dnsmasq"
		cve_name = "dnsmasq"
		open_source = true
		version = "2.64"
	strings:
		$a = "2.64" ascii wide
	condition:
		$a and dnsmasq
}
rule dnsmasq_2_65 : v2_65
{
	meta:
		name = "dnsmasq"
		cve_name = "dnsmasq"
		open_source = true
		version = "2.65"
	strings:
		$a = "2.65" ascii wide
	condition:
		$a and dnsmasq
}
rule dnsmasq_2_66 : v2_66
{
	meta:
		name = "dnsmasq"
		cve_name = "dnsmasq"
		open_source = true
		version = "2.66"
	strings:
		$a = "2.66" ascii wide
	condition:
		$a and dnsmasq
}
rule dnsmasq_2_67 : v2_67
{
	meta:
		name = "dnsmasq"
		cve_name = "dnsmasq"
		open_source = true
		version = "2.67"
	strings:
		$a = "2.67" ascii wide
	condition:
		$a and dnsmasq
}
rule dnsmasq_2_68 : v2_68
{
	meta:
		name = "dnsmasq"
		cve_name = "dnsmasq"
		open_source = true
		version = "2.68"
	strings:
		$a = "2.68" ascii wide
	condition:
		$a and dnsmasq
}
rule dnsmasq_2_69 : v2_69
{
	meta:
		name = "dnsmasq"
		cve_name = "dnsmasq"
		open_source = true
		version = "2.69"
	strings:
		$a = "2.69" ascii wide
	condition:
		$a and dnsmasq
}
rule dnsmasq_2_70 : v2_70
{
	meta:
		name = "dnsmasq"
		cve_name = "dnsmasq"
		open_source = true
		version = "2.70"
	strings:
		$a = "2.70" ascii wide
	condition:
		$a and dnsmasq
}
rule dnsmasq_2_71 : v2_71
{
	meta:
		name = "dnsmasq"
		cve_name = "dnsmasq"
		open_source = true
		version = "2.71"
	strings:
		$a = "2.71" ascii wide
	condition:
		$a and dnsmasq
}
rule dnsmasq_2_72 : v2_72
{
	meta:
		name = "dnsmasq"
		cve_name = "dnsmasq"
		open_source = true
		version = "2.72"
	strings:
		$a = "2.72" ascii wide
	condition:
		$a and dnsmasq
}
rule dnsmasq_2_73: v2_73
{
	meta:
		name = "dnsmasq"
		cve_name = "dnsmasq"
		open_source = true
		version = "2.73"
	strings:
		$a = "2.73" ascii wide
	condition:
		$a and dnsmasq
}
rule dnsmasq_2_74 : v2_74
{
	meta:
		name = "dnsmasq"
		cve_name = "dnsmasq"
		open_source = true
		version = "2.74"
	strings:
		$a = "2.74" ascii wide
	condition:
		$a and dnsmasq
}
rule dnsmasq_2_75 : v2_75
{
	meta:
		name = "dnsmasq"
		cve_name = "dnsmasq"
		open_source = true
		version = "2.75"
	strings:
		$a = "2.75" ascii wide
	condition:
		$a and dnsmasq
}
rule dnsmasq_2_76 : v2_76
{
	meta:
		name = "dnsmasq"
		cve_name = "dnsmasq"
		open_source = true
		version = "2.76"
	strings:
		$a = "2.76" ascii wide
	condition:
		$a and dnsmasq
}
rule dnsmasq_2_77 : v2_77
{
	meta:
		name = "dnsmasq"
		cve_name = "dnsmasq"
		open_source = true
		version = "2.77"
	strings:
		$a = "2.77" ascii wide
	condition:
		$a and dnsmasq
}
rule dnsmasq_2_78 : v2_78
{
	meta:
		name = "dnsmasq"
		cve_name = "dnsmasq"
		open_source = true
		version = "2.78"
	strings:
		$a = "2.78" ascii wide
	condition:
		$a and dnsmasq
}
private rule dropbear : dropbear
{
	meta:
		name = "Dropbear"
		cve_name = "dropbear_ssh"
		open_source = true
		url = "https://matt.ucc.asn.au/dropbear/dropbear.html"
	strings:
		$a = "dropbear" nocase ascii wide
	condition:
		$a
}
rule dropbear_2016_74 : v2016_74
{
	meta:
		name = "Dropbear"
		cve_name = "dropbear_ssh"
		version = "2016.74"
	strings:
		$a = "2016.74" ascii wide
	condition:
		$a and dropbear
}
rule dropbear_2017_75 : v2017_75
{
	meta:
		name = "Dropbear"
		cve_name = "dropbear_ssh"
		version = "2017.75"
	strings:
		$a = "2017.75" ascii wide
	condition:
		$a and dropbear
}
private rule lighttpd
{
	meta:
		name = "lighttpd"
		cve_name = "lighttpd"
		open_source = true
	strings:
		$a = /lighttpd.{1}(\d+\.\d+\.\d+){1}/ nocase ascii wide
	condition:
		$a 
}
rule lighttpd_1_4_14 : v1_4_14
{
	meta:
		name = "lighttpd"
		cve_name = "lighttpd"
		open_source = true
		version = "1.4.14"
	strings:
		$a = "1.4.14" nocase ascii wide
	condition:
		$a and lighttpd
}
rule lighttpd_1_4_15 : v1_4_15
{
	meta:
		name = "lighttpd"
		cve_name = "lighttpd"
		open_source = true
		version = "1.4.15"
	strings:
		$a = "1.4.15" nocase ascii wide
	condition:
		$a and lighttpd
}
rule lighttpd_1_4_16: v1_4_16
{
	meta:
		name = "lighttpd"
		cve_name = "lighttpd"
		open_source = true
		version = "1.4.16"
	strings:
		$a = "1.4.16" nocase ascii wide
	condition:
		$a and lighttpd
}
rule lighttpd_1_4_17 : v1_4_17
{
	meta:
		name = "lighttpd"
		cve_name = "lighttpd"
		open_source = true
		version = "1.4.17"
	strings:
		$a = "1.4.17" nocase ascii wide
	condition:
		$a and lighttpd
}
rule lighttpd_1_4_18 : v1_4_18
{
	meta:
		name = "lighttpd"
		cve_name = "lighttpd"
		open_source = true
		version = "1.4.18"
	strings:
		$a = "1.4.18" nocase ascii wide
	condition:
		$a and lighttpd
}
rule lighttpd_1_4_19 : v1_4_19
{
	meta:
		name = "lighttpd"
		cve_name = "lighttpd"
		open_source = true
		version = "1.4.19"
	strings:
		$a = "1.4.19" nocase ascii wide
	condition:
		$a and lighttpd 
}
rule lighttpd_1_4_20 : v1_4_20
{
	meta:
		name = "lighttpd"
		cve_name = "lighttpd"
		open_source = true
		version = "1.4.20"
	strings:
		$a = "1.4.20" nocase ascii wide
	condition:
		$a and lighttpd
}
rule lighttpd_1_4_21 : v1_4_21
{
	meta:
		name = "lighttpd"
		cve_name = "lighttpd"
		open_source = true
		version = "1.4.21"
	strings:
		$a = "1.4.21" nocase ascii wide
	condition:
		$a and lighttpd
}
rule lighttpd_1_4_22 : v1_4_22
{
	meta:
		name = "lighttpd"
		cve_name = "lighttpd"
		open_source = true
		version = "1.4.22"
	strings:
		$a = "1.4.22" nocase ascii wide
	condition:
		$a and lighttpd 
}
rule lighttpd_1_4_23 : v1_4_23
{
	meta:
		name = "lighttpd"
		cve_name = "lighttpd"
		open_source = true
		version = "1.4.23"
	strings:
		$a = "1.4.23" nocase ascii wide
	condition:
		$a and lighttpd
}
rule lighttpd_1_4_24 : v1_4_24
{
	meta:
		name = "lighttpd"
		cve_name = "lighttpd"
		open_source = true
		version = "1.4.24"
	strings:
		$a = "1.4.24" nocase ascii wide
	condition:
		$a and lighttpd
}
rule lighttpd_1_4_25 : v1_4_25
{
	meta:
		name = "lighttpd"
		cve_name = "lighttpd"
		open_source = true
		version = "1.4.25"
	strings:
		$a = "1.4.25" nocase ascii wide
	condition:
		$a and lighttpd
}
rule lighttpd_1_4_26 : v1_4_26
{
	meta:
		name = "lighttpd"
		cve_name = "lighttpd"
		open_source = true
		version = "1.4.26"
	strings:
		$a = "1.4.26" nocase ascii wide
	condition:
		$a and lighttpd
}
rule lighttpd_1_4_27 : v1_4_27
{
	meta:
		name = "lighttpd"
		cve_name = "lighttpd"
		open_source = true
		version = "1.4.27"
	strings:
		$a = "1.4.27" nocase ascii wide
	condition:
		$a and lighttpd
}
rule lighttpd_1_4_28 : v1_4_28
{
	meta:
		name = "lighttpd"
		cve_name = "lighttpd"
		open_source = true
		version = "1.4.28"
	strings:
		$a = "1.4.28" nocase ascii wide
	condition:
		$a and lighttpd
}
rule lighttpd_1_4_29 : v1_4_29
{
	meta:
		name = "lighttpd"
		cve_name = "lighttpd"
		open_source = true
		version = "1.4.29"
	strings:
		$a = "1.4.29" nocase ascii wide
	condition:
		$a and lighttpd
}
rule lighttpd_1_4_30 : v1_4_30
{
	meta:
		name = "lighttpd"
		cve_name = "lighttpd"
		open_source = true
		version = "1.4.30"
	strings:
		$a = "1.4.30" nocase ascii wide
	condition:
		$a and lighttpd
}
rule lighttpd_1_4_31 : v1_4_31
{
	meta:
		name = "lighttpd"
		cve_name = "lighttpd"
		open_source = true
		version = "1.4.31"
	strings:
		$a = "1.4.31" nocase ascii wide
	condition:
		$a and lighttpd
}
rule lighttpd_1_4_32 : v1_4_32
{
	meta:
		name = "lighttpd"
		cve_name = "lighttpd"
		open_source = true
		version = "1.4.32"
	strings:
		$a = "1.4.32" nocase ascii wide
	condition:
		$a and lighttpd
}
rule lighttpd_1_4_33 : v1_4_33
{
	meta:
		name = "lighttpd"
		cve_name = "lighttpd"
		open_source = true
		version = "1.4.33"
	strings:
		$a = "1.4.33" nocase ascii wide
	condition:
		$a and lighttpd
}
rule lighttpd_1_4_34 : v1_4_34
{
	meta:
		name = "lighttpd"
		cve_name = "lighttpd"
		open_source = true
		version = "1.4.34"
	strings:
		$a = "1.4.34" nocase ascii wide
	condition:
		$a and lighttpd
}
rule lighttpd_1_4_35 : v1_4_35
{
	meta:
		name = "lighttpd"
		cve_name = "lighttpd"
		open_source = true
		version = "1.4.35"
	strings:
		$a = "1.4.35" nocase ascii wide
	condition:
		$a and lighttpd
}
rule lighttpd_1_4_36 : v1_4_36
{
	meta:
		name = "lighttpd"
		cve_name = "lighttpd"
		open_source = true
		version = "1.4.36"
	strings:
		$a = "1.4.36" nocase ascii wide
	condition:
		$a and lighttpd
}
rule lighttpd_1_4_37 : v1_4_37
{
	meta:
		name = "lighttpd"
		cve_name = "lighttpd"
		open_source = true
		version = "1.4.37"
	strings:
		$a = "1.4.37" nocase ascii wide
	condition:
		$a and lighttpd
}
rule lighttpd_1_4_38 : v1_4_38
{
	meta:
		name = "lighttpd"
		cve_name = "lighttpd"
		open_source = true
		version = "1.4.38"
	strings:
		$a = "1.4.38" nocase ascii wide
	condition:
		$a and lighttpd
}
rule lighttpd_1_4_39 : v1_4_39
{
	meta:
		name = "lighttpd"
		cve_name = "lighttpd"
		open_source = true
		version = "1.4.39"
	strings:
		$a = "1.4.39" nocase ascii wide
	condition:
		$a and lighttpd
}
rule lighttpd_1_4_40 : v1_4_40
{
	meta:
		name = "lighttpd"
		cve_name = "lighttpd"
		open_source = true
		version = "1.4.40"
	strings:
		$a = "1.4.40" nocase ascii wide
	condition:
		$a and lighttpd
}
rule lighttpd_1_4_41 : v1_4_41
{
	meta:
		name = "lighttpd"
		cve_name = "lighttpd"
		open_source = true
		version = "1.4.41"
	strings:
		$a = "1.4.41" nocase ascii wide
	condition:
		$a and lighttpd
}
rule lighttpd_1_4_42 : v1_4_42
{
	meta:
		name = "lighttpd"
		cve_name = "lighttpd"
		open_source = true
		version = "1.4.42"
	strings:
		$a = "1.4.42" nocase ascii wide
	condition:
		$a and lighttpd
}
rule lighttpd_1_4_43 : v1_4_43
{
	meta:
		name = "lighttpd"
		cve_name = "lighttpd"
		open_source = true
		version = "1.4.43"
	strings:
		$a = "1.4.43" nocase ascii wide
	condition:
		$a and lighttpd
}
rule lighttpd_1_4_44 : v1_4_44
{
	meta:
		name = "lighttpd"
		cve_name = "lighttpd"
		open_source = true
		version = "1.4."
	strings:
		$a = "1.4.44" nocase ascii wide
	condition:
		$a and lighttpd
}
rule lighttpd_1_4_45 : v1_4_45
{
	meta:
		name = "lighttpd"
		cve_name = "lighttpd"
		open_source = true
		version = "1.4.45"
	strings:
		$a = "1.4.45" nocase ascii wide
	condition:
		$a and lighttpd
}
rule lighttpd_1_4_46 : v1_4_46
{
	meta:
		name = "lighttpd"
		cve_name = "lighttpd"
		open_source = true
		version = "1.4.46"
	strings:
		$a = "1.4.46" nocase ascii wide
	condition:
		$a and lighttpd
}
rule lighttpd_1_4_47 : v1_4_47
{
	meta:
		name = "lighttpd"
		cve_name = "lighttpd"
		open_source = true
		version = "1.4.47"
	strings:
		$a = "1.4.47" nocase ascii wide
	condition:
		$a and lighttpd
}
rule lighttpd_1_4_48 : v1_4_48
{
	meta:
		name = "lighttpd"
		cve_name = "lighttpd"
		open_source = true
		version = "1.4.48"
	strings:
		$a = "1.4.48" nocase ascii wide
	condition:
		$a and lighttpd
}
private rule openssl
{
	meta:
		name = "OpenSSL"
		cve_name = "openssl"
		open_source = true
	strings:
		$a = /openssl( \d+\.\d+\.\d+[a-z]?)/ nocase ascii wide
	condition:
		$a
}
rule openssl_1_0_1_beta1 : v1_0_1_beta1
{
	meta:
		name = "openssl"
		cve_name = "openssl"
		version = "1.0.1-beta1"
	strings:
		$a = "1.0.1-beta1" nocase ascii
	condition:
		$a and openssl
}
rule openssl_1_0_1_beta2 : v1_0_1_beta2
{
	meta:
		name = "openssl"
		cve_name = "openssl"
		version = "1.0.1-beta2"
	strings:
		$a = "1.0.1-beta2" nocase ascii
	condition:
		$a and openssl
}
rule openssl_1_0_1_beta3 : v1_0_1_beta3
{
	meta:
		name = "openssl"
		cve_name = "openssl"
		version = "1.0.1-beta3"
	strings:
		$a = "1.0.1-beta3" nocase ascii
	condition:
		$a and openssl
}
rule openssl_1_0_1 : v1_0_1
{
	meta:
		name = "openssl"
		cve_name = "openssl"
		version = "1.0.1"
	strings:
		$a = "1.0.1[^a-z]" nocase ascii
	condition:
		$a and openssl
}
rule openssl_1_0_1a : v1_0_1a
{
	meta:
		name = "openssl"
		cve_name = "openssl"
		version = "1.0.1a"
	strings:
		$a = "1.0.1a" nocase ascii
	condition:
		$a and openssl
}
rule openssl_1_0_1b : v1_0_1b
{
	meta:
		name = "openssl"
		cve_name = "openssl"
		version = "1.0.1b"
	strings:
		$a = "1.0.1b" nocase ascii
	condition:
		$a and openssl
}
rule openssl_1_0_1c : v1_0_1c
{
	meta:
		name = "openssl"
		cve_name = "openssl"
		version = "1.0.1c"
	strings:
		$a = "1.0.1c" nocase ascii
	condition:
		$a and openssl
}
rule openssl_1_0_1d : v1_0_1d
{
	meta:
		name = "openssl"
		cve_name = "openssl"
		version = "1.0.1d"
	strings:
		$a = "1.0.1d" nocase ascii
	condition:
		$a and openssl
}
rule openssl_1_0_1e : v1_0_1e
{
	meta:
		name = "openssl"
		cve_name = "openssl"
		version = "1.0.1e"
	strings:
		$a = "1.0.1e" nocase ascii
	condition:
		$a and openssl
}
rule openssl_1_0_1f : v1_0_1f
{
	meta:
		name = "openssl"
		cve_name = "openssl"
		version = "1.0.1f"
	strings:
		$a = "1.0.1f" nocase ascii
	condition:
		$a and openssl
}
rule openssl_1_0_1g : v1_0_1g
{
	meta:
		name = "openssl"
		cve_name = "openssl"
		version = "1.0.1g"
	strings:
		$a = "1.0.1g" nocase ascii
	condition:
		$a and openssl
}
rule openssl_1_0_1h : v1_0_1h
{
	meta:
		name = "openssl"
		cve_name = "openssl"
		version = "1.0.1h"
	strings:
		$a = "1.0.1h" nocase ascii
	condition:
		$a and openssl
}
rule openssl_1_0_1i : v1_0_1i
{
	meta:
		name = "openssl"
		cve_name = "openssl"
		version = "1.0.1i"
	strings:
		$a = "1.0.1i" nocase ascii
	condition:
		$a and openssl
}
rule openssl_1_0_1j : v1_0_1j
{
	meta:
		name = "openssl"
		cve_name = "openssl"
		version = "1.0.1j"
	strings:
		$a = "1.0.1j" nocase ascii
	condition:
		$a and openssl
}
rule openssl_1_0_1k : v1_0_1k
{
	meta:
		name = "openssl"
		cve_name = "openssl"
		version = "1.0.1k"
	strings:
		$a = "1.0.1k" nocase ascii
	condition:
		$a and openssl
}
rule openssl_1_0_1l : v1_0_1l
{
	meta:
		name = "openssl"
		cve_name = "openssl"
		version = "1.0.1l"
	strings:
		$a = "1.0.1l" nocase ascii
	condition:
		$a and openssl
}
rule openssl_1_0_1m : v1_0_1m
{
	meta:
		name = "openssl"
		cve_name = "openssl"
		version = "1.0.1m"
	strings:
		$a = "1.0.1m" nocase ascii
	condition:
		$a and openssl
}
rule openssl_1_0_1n : v1_0_1n
{
	meta:
		name = "openssl"
		cve_name = "openssl"
		version = "1.0.1n"
	strings:
		$a = "1.0.1n" nocase ascii
	condition:
		$a and openssl
}
rule openssl_1_0_1o : v1_0_1o
{
	meta:
		name = "openssl"
		cve_name = "openssl"
		version = "1.0.1o"
	strings:
		$a = "1.0.1o" nocase ascii
	condition:
		$a and openssl
}
rule openssl_1_0_1p : v1_0_1p
{
	meta:
		name = "openssl"
		cve_name = "openssl"
		version = "1.0.1p"
	strings:
		$a = "1.0.1p" nocase ascii
	condition:
		$a and openssl
}
rule openssl_1_0_1q : v1_0_1q
{
	meta:
		name = "openssl"
		cve_name = "openssl"
		version = "1.0.1q"
	strings:
		$a = "1.0.1q" nocase ascii
	condition:
		$a and openssl
}
rule openssl_1_0_1r : v1_0_1r
{
	meta:
		name = "openssl"
		cve_name = "openssl"
		version = "1.0.1r"
	strings:
		$a = "1.0.1r" nocase ascii
	condition:
		$a and openssl
}
rule openssl_1_0_1s : v1_0_1s
{
	meta:
		name = "openssl"
		cve_name = "openssl"
		version = "1.0.1s"
	strings:
		$a = "1.0.1s" nocase ascii
	condition:
		$a and openssl
}
rule openssl_1_0_1t : v1_0_1t
{
	meta:
		name = "openssl"
		cve_name = "openssl"
		version = "1.0.1t"
	strings:
		$a = "1.0.1t" nocase ascii
	condition:
		$a and openssl
}
rule openssl_1_0_1u : v1_0_1u
{
	meta:
		name = "openssl"
		cve_name = "openssl"
		version = "1.0.1u"
	strings:
		$a = "1.0.1u" nocase ascii
	condition:
		$a and openssl
}
rule openssl_1_0_2_beta1 : v1_0_2_beta1
{
	meta:
		name = "openssl"
		cve_name = "openssl"
		version = "1.0.2-beta1"
	strings:
		$a = "1.0.2-beta1" nocase ascii
	condition:
		$a and openssl
}
rule openssl_1_0_2_beta2 : v1_0_2_beta2
{
	meta:
		name = "openssl"
		cve_name = "openssl"
		version = "1.0.2-beta2"
	strings:
		$a = "1.0.2-beta2" nocase ascii
	condition:
		$a and openssl
}
rule openssl_1_0_2_beta3 : v1_0_2_beta3
{
	meta:
		name = "openssl"
		cve_name = "openssl"
		version = "1.0.2-beta3"
	strings:
		$a = "1.0.2-beta3" nocase ascii
	condition:
		$a and openssl
}
rule openssl_1_0_2 : v1_0_2
{
	meta:
		name = "openssl"
		cve_name = "openssl"
		version = "1.0.2"
	strings:
		$a = "1.0.2[^a-z]" nocase ascii
	condition:
		$a and openssl
}
rule openssl_1_0_2a : v1_0_2a
{
	meta:
		name = "openssl"
		cve_name = "openssl"
		version = "1.0.2a"
	strings:
		$a = "1.0.2a" nocase ascii
	condition:
		$a and openssl
}
rule openssl_1_0_2b : v1_0_2b
{
	meta:
		name = "openssl"
		cve_name = "openssl"
		version = "1.0.2b"
	strings:
		$a = "1.0.2b" nocase ascii
	condition:
		$a and openssl
}
rule openssl_1_0_2c : v1_0_2c
{
	meta:
		name = "openssl"
		cve_name = "openssl"
		version = "1.0.2c"
	strings:
		$a = "1.0.2c" nocase ascii
	condition:
		$a and openssl
}
rule openssl_1_0_2d : v1_0_2d
{
	meta:
		name = "openssl"
		cve_name = "openssl"
		version = "1.0.2d"
	strings:
		$a = "1.0.2d" nocase ascii
	condition:
		$a and openssl
}
rule openssl_1_0_2e : v1_0_2e
{
	meta:
		name = "openssl"
		cve_name = "openssl"
		version = "1.0.2e"
	strings:
		$a = "1.0.2e" nocase ascii
	condition:
		$a and openssl
}
rule openssl_1_0_2f : v1_0_2f
{
	meta:
		name = "openssl"
		cve_name = "openssl"
		version = "1.0.2f"
	strings:
		$a = "1.0.2f" nocase ascii
	condition:
		$a and openssl
}
rule openssl_1_0_2g : v1_0_2g
{
	meta:
		name = "openssl"
		cve_name = "openssl"
		version = "1.0.2g"
	strings:
		$a = "1.0.2g" nocase ascii
	condition:
		$a and openssl
}
rule openssl_1_0_2h : v1_0_2h
{
	meta:
		name = "openssl"
		cve_name = "openssl"
		version = "1.0.2h"
	strings:
		$a = "1.0.2h" nocase ascii
	condition:
		$a and openssl
}
rule openssl_1_0_2i : v1_0_2i
{
	meta:
		name = "openssl"
		cve_name = "openssl"
		version = "1.0.2i"
	strings:
		$a = "1.0.2i" nocase ascii
	condition:
		$a and openssl
}
rule openssl_1_0_2j : v1_0_2j
{
	meta:
		name = "openssl"
		cve_name = "openssl"
		version = "1.0.2j"
	strings:
		$a = "1.0.2j" nocase ascii
	condition:
		$a and openssl
}
rule openssl_1_0_2k : v1_0_2k
{
	meta:
		name = "openssl"
		cve_name = "openssl"
		version = "1.0.2k"
	strings:
		$a = "1.0.2k" nocase ascii
	condition:
		$a and openssl
}
rule openssl_1_0_2l : v1_0_2l
{
	meta:
		name = "openssl"
		cve_name = "openssl"
		version = "1.0.2l"
	strings:
		$a = "1.0.2l" nocase ascii
	condition:
		$a and openssl
}
rule openssl_1_1_0_pre1 : v1_1_0_pre1
{
	meta:
		name = "openssl"
		cve_name = "openssl"
		version = "1.1.0-pre1"
	strings:
		$a = "1.1.0-pre1" nocase ascii
	condition:
		$a and openssl
}
rule openssl_1_1_0_pre2 : v1_1_0_pre2
{
	meta:
		name = "openssl"
		cve_name = "openssl"
		version = "1.1.0-pre2"
	strings:
		$a = "1.1.0-pre2" nocase ascii
	condition:
		$a and openssl
}
rule openssl_1_1_0_pre3 : v1_1_0_pre3
{
	meta:
		name = "openssl"
		cve_name = "openssl"
		version = "1.1.0-pre3"
	strings:
		$a = "1.1.0-pre3" nocase ascii
	condition:
		$a and openssl
}
rule openssl_1_1_0 : v1_1_0
{
	meta:
		name = "openssl"
		cve_name = "openssl"
		version = "1.1.0"
	strings:
		$a = "1.1.0[^a-z]" nocase ascii
	condition:
		$a and openssl
}
rule openssl_1_1_0a : v1_1_0a
{
	meta:
		name = "openssl"
		cve_name = "openssl"
		version = "1.1.0a"
	strings:
		$a = "1.1.0a" nocase ascii
	condition:
		$a and openssl
}
rule openssl_1_1_0b : v1_1_0b
{
	meta:
		name = "openssl"
		cve_name = "openssl"
		version = "1.1.0b"
	strings:
		$a = "1.1.0b" nocase ascii
	condition:
		$a and openssl
}
rule openssl_1_1_0c : v1_1_0c
{
	meta:
		name = "openssl"
		cve_name = "openssl"
		version = "1.1.0c"
	strings:
		$a = "1.1.0c" nocase ascii
	condition:
		$a and openssl
}
rule openssl_1_1_0d : v1_1_0d
{
	meta:
		name = "openssl"
		cve_name = "openssl"
		version = "1.1.0d"
	strings:
		$a = "1.1.0d" nocase ascii
	condition:
		$a and openssl
}
rule openssl_1_1_0e : v1_1_0e
{
	meta:
		name = "openssl"
		cve_name = "openssl"
		version = "1.1.0e"
	strings:
		$a = "1.1.0e" nocase ascii
	condition:
		$a and openssl
}
rule openssl_1_1_0f : v1_1_0f
{
	meta:
		name = "openssl"
		cve_name = "openssl"
		version = "1.1.0f"
	strings:
		$a = "1.1.0f" nocase ascii
	condition:
		$a and openssl
}
