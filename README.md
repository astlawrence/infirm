# infirm

This tool will recursively extract a firmware image, walk its file system path searching for binaries and then compare those binaries to known fingerprints, YARA rules or fuzzy strings. If any extracted binaries match, the tool will search NVD CVE data and report known vulnerabilities.

### Requirements

>    $ sudo apt-get install binwalk sqlite3 python3 python3-pip3
 
>    $ pip3 install docopt filemagic prettytable fuzzywuzzy yara 

### Usage
On first use, initialize the database by invoking the following command where YEAR represents the starting year to present. This will download NVD CVE data and initialize the database for the period of years between the starting year and present. For example, supplying the argument 2015 will result in NVD CVE data being downloaded and processed for the years 2015-2017.
>    $ python3 infirm.py --init YEAR
 
>    $ python3 infirm.py [-f|-t] [-r] FIRMWARE

### Help
>    $ python3 infirm.py --help  
>
>    Usage:  
>        infirm.py [-f|-t] [-r] FIRMWARE  
>        infirm.py (-i | --init) YEAR  
>        infirm.py (-l | --locals)  
>        infirm.py (-c | --clear) [binaries|locals|cve]  
>        infirm.py (-u | --update) [cve|binaries]  
>        infirm.py (-h | --help)  
>        infirm.py --version  
>     
>    Options:  
>        -f                Perform a fast analysis using YARA rules.  
>        -t                Perform a thorough analysis using precomputed binary data (default).  
>        -r                Retain analyzed binary metadata in local database.  
>        -i --init         Initialize the database with NVD CVE data starting from the year provided to current.  
>        -l --locals       Rerun analysis on binaries in local database.  
>        -c --clear        Delete all data in the binaries, locals, or cve database tables.  
>        -u --update       Update CVE or binaries data to current.  
>        -h --help         Show this screen.  
>        --version         Show version.  

